#!/bin/bash
set -x
. ubuntu-common.sh
cd frontend

if [[ "$FRONTEND_CLEAN" = true ]]; then
  rm -rf node_modules/ bower_components/
  if [[ "$(id -u)" = 0 ]]; then
    npm --unsafe-perm cache clean
    bower --allow-root --config.interactive=false cache clean
  else
    npm cache clean
    bower --config.interactive=false cache clean
  fi
fi

if [[ "$(id -u)" = 0 ]]; then
  retry 3 "npm install --unsafe-perm"
else
  retry 3 "npm install"
fi

if [[ "$(id -u)" = 0 ]]; then
  retry 3 "bower install --allow-root --config.interactive=false"
else
  retry 3 "bower install --config.interactive=false"
fi
