require 'erb'
require 'fileutils'
include FileUtils

$trust_hosts={}
GIT=File.join(File.dirname(__FILE__), 'git')
HOME=ENV['HOME']

class Repo
  def initialize repo
    @repo = repo
    trust
  end
  def uri
    @repo
  end
  def repo_dir
    @repo.split(':').last
  end
  def server
    uri.gsub(/[^a-z\-\_]/i, '_')
  end
  def host
    @repo.split(':').first.split('@').last
  end
  def trust
    return if $trust_hosts[host]
    if File.read("#{HOME}/.ssh/known_hosts").index(host).nil?
      sh "ssh-keyscan #{host} >> #{HOME}/.ssh/known_hosts"
      $trust_hosts[host] = true
    end
  end
end

class Backup
  def initialize name, email, file
    @name = name
    @email = email
    raise ArgumentError, "file does not exist: #{file}" unless File.exist?(file)
    @file = file
  end
  def perform hash
    hash.each do |key, value|
      from = Repo.new(key)
      to = Repo.new(value)
      puts "#{from.uri} => #{to.uri}"

      from_repo_dir = "#{GIT}/#{@name}/from/#{from.repo_dir}"
      mkdir_p from_repo_dir unless File.exist?(from_repo_dir)
      sh "git clone #{from.uri} #{from_repo_dir}" unless File.exist?("#{from_repo_dir}/.git")
      cd from_repo_dir

      sh "git fetch --tags origin"
      sh "git pull --rebase origin master"
      sh "git config core.autocrlf input"
      sh "git config user.email #{@email}"

      # copy only changed files with rsync
      to_repo_dir = "#{GIT}/#{@name}/to/#{to.repo_dir}"
      mkdir_p to_repo_dir unless File.exist?(to_repo_dir)
      sh "git clone #{to.uri} #{to_repo_dir}" unless File.exist?("#{to_repo_dir}/.git")
      cd to_repo_dir
      sh %|rsync -lptvc --files-from="#{@file}" #{from_repo_dir}/ #{to_repo_dir}/|
      any_changes = `git status --porcelain`.strip.length > 0
      if any_changes
        sh "git add -A"
        sh "git commit -m 'update'"
      # Commenting out this for now, since we are changing the workflow to run app-image-no-changes every time.
      # else
      #   sh %|curl -i -s "https://patcoll:1a779e992abb24568aeb1a5cd0ca7403@automate.luma-institute.com/job/app-image-no-changes/build?token=gYnEegPitid7HQQWVXtDxXrdrzFmW8FDVRNLdCMwfgaFtxaAyN"|
      end
      sh "git pull --rebase origin master"
      sh "git push --all origin"

      # for stdout spacing
      puts
    end
  end
end

task :default do
  backup = Backup.new "luma", "pat@luma-institute.com", File.join(File.dirname(__FILE__), 'include/app-image.txt')
  backup.perform({
    "git@github.com:luma-institute/luma_app.git" => "git@github.com:luma-institute/app-image.git",
  })
end

