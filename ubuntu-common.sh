#!/bin/bash
retry_forever() {
  local sleeptime=60
  while ! eval $@
  do
    sleep $sleeptime
  done
}

retry() {
  local n=0
  local sleeptime=60
  local tries=$1
  local cmd="${@:2}"
  [[ $# -le 1 ]] && {
    echo "Usage retry <retry_number> '<Command>'"
  }
  until [[ $n -ge $tries ]]
  do
    eval $cmd && break || {
      ((n++))
      echo "[retry]: '$cmd' failed. sleeping $sleeptime seconds then trying again..."
      sleep $sleeptime
    }
  done
}
