# -*- mode: conf -*-
# # vi: set ft=conf :
# in gitignore format for use with rsync
# apt-get deps
/Dockerfile
/ubuntu-nodesource-4.x.sh
/nvm-install.sh
/ubuntu-common.sh
/ubuntu-bootstrap-deps.sh

# app deps
/ubuntu-bootstrap-app-deps.sh
/Gemfile
/Gemfile.lock
/.ruby-version
/package.json
/postinstall.sh
/frontend/package.json
/frontend/bower.json
/frontend/postinstall.sh
