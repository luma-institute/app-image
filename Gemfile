source 'https://rubygems.org'
if File.exist?(File.expand_path('../.ruby-version', __FILE__))
  ruby File.read(File.expand_path('../.ruby-version', __FILE__)).strip
end

gem 'rake'
gem 'rails', '~> 4.2.0'
gem 'pg'
gem 'active_model_serializers'
gem 'acts-as-taggable-on', '~> 3.4'
gem 'bitfields'
gem 'cancancan'
gem 'cocaine'
gem 'devise', '~> 3.5.2'
gem 'grape', '~> 0.14.0'
gem 'grape-cancan'
gem 'mailchimp-api', require: 'mailchimp'
gem 'mandrill-api'
# needed for aws-sdk v2, change to 5.0.x series when that comes out
gem 'paperclip', github: 'thoughtbot/paperclip', ref: 'master'
gem 'pg_search'
gem 'puma'
gem 'rack-cors', require: 'rack/cors'
gem 'rack-timeout'
gem 'gabba', github: 'ardell/gabba'
gem 'sproutvideo-rb'
gem 'sidekiq', '~> 4.1.0'
gem 'sinatra', require: nil # for sidekiq
gem 'seedbank'
gem 'seed-fu'
gem 'friendly_id', '~> 5.1.0'
gem 'torid'
gem 'rest-client', require: nil # for fastly script to clear cdn
gem 'redis'
gem 'redis-rails'
gem 'sidekiq-superworker'
gem 'keen'
gem 'hashie'
gem 'ember-cli-rails', '~> 0.7.1'
gem 'analytics-ruby', require: 'segment'
gem 'pdf-reader'
gem 'crono'
gem 'ruby-saml'

group :production do
  gem 'rails_12factor'
  gem 'aws-sdk', '~> 2.2.5'
  gem 'sentry-raven'
  gem 'heroku-deflater'
  gem 'skylight'
end

group :development do
  gem 'dotenv', require: nil
  gem 'git', require: nil
  gem 'foreman'
  gem 'versionomy', '~> 0.5.0'
  gem 'guard-rspec', require: nil
  gem 'derailed'
  gem 'stackprof'
end

group :development, :test do
  gem 'brakeman', require: nil
  # gem 'bundler-audit', require: nil
  # gem 'byebug'
  # gem 'factory_girl_rails'
  # gem 'pry-rails'
  gem 'rubocop', require: nil

  gem 'rspec', '~> 3.4.0'
  gem 'rspec-rails', '~> 3.4.0'
  gem 'rspec-collection_matchers'
  gem 'parallel_tests', '~> 2.4.0'
  gem 'rspec-retry', require: nil

  # extras for local testing
  # gem 'capybara-webkit', '~> 1.8.0'
  gem 'capybara', '~> 2.6.2'
  gem 'selenium-webdriver', '~> 2.52.0'
  gem 'database_cleaner'
  gem 'poltergeist'
  # gem 'phantomjs'
end

group :test do
  gem 'rspec-sidekiq', require: nil
  gem 'simplecov', require: nil
  gem 'simplecov-lcov', require: nil
  # gem 'coveralls', require: nil
  # gem 'formulaic'
  # gem 'launchy'
  # gem 'timecop'
  # gem 'webmock'
end
