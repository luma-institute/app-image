#!/bin/bash
. ubuntu-common.sh
set -x

if [[ "$FRONTEND_CLEAN" = true ]]; then
  rm -rf node_modules/
  if [[ "$(id -u)" = 0 ]]; then
    npm --unsafe-perm cache clean
  else
    npm cache clean
  fi
fi

if [[ "$(id -u)" = 0 ]]; then
  retry 3 "npm install --unsafe-perm"
else
  retry 3 "npm install"
fi
retry ${BUNDLE_RETRY:-2} "bundle install --jobs=${BUNDLE_JOBS:-2} --retry=${BUNDLE_RETRY:-2}"
