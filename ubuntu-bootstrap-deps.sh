#!/bin/bash
. ubuntu-common.sh
set -ev
# option to pin to specific debian mirror
sed -i 's=httpredir.debian.org=http.us.debian.org=' /etc/apt/sources.list
retry 3 "apt-get update -y"

# system-level dependencies for app language libraries
packages="build-essential zlibc zlib1g zlib1g-dev libxml2-dev libxslt1-dev libgdbm-dev qt4-default libqt4-dev libqtwebkit-dev xvfb postgresql-server-dev-all postgresql-client"
retry 3 "apt-get install -y $packages"

# NVM and NodeJS
# nvm dependencies
nvm_packages="git-core build-essential g++ libssl-dev curl wget apache2-utils libxml2-dev"
retry 3 "apt-get install -y $nvm_packages"
# install nvm
# silence verbosity since this is really noisy :)
set +v
retry 3 "env ./nvm-install.sh"
NODE_VERSION=$(cat package.json | grep '"node":' | sed 's="node":==' | awk '{print $1}' | sed 's="==g' | sed 's=,==g')
# install correct node version
. $NVM_DIR/nvm.sh
nvm install v$NODE_VERSION
nvm use v$NODE_VERSION
nvm alias default v$NODE_VERSION
set -v
ln -nfs $NVM_DIR/versions/node/v$NODE_VERSION/bin/node /usr/local/bin/node
ln -nfs $NVM_DIR/versions/node/v$NODE_VERSION/bin/npm /usr/local/bin/npm
ln -nfs $NVM_DIR/versions/node/v$NODE_VERSION/lib/node_modules /usr/local/lib/node_modules

# npm dependencies for frontend
if [[ ! -r /usr/local/bin/bower ]]; then
  retry 3 "npm install -g bower"
  ln -nfs $NVM_DIR/versions/node/v$NODE_VERSION/bin/bower /usr/local/bin/bower
fi

if [[ ! -r /usr/local/bin/phantomjs ]]; then
  retry 3 "npm install -g phantomjs-prebuilt@2.1.3"
  ln -nfs $NVM_DIR/versions/node/v$NODE_VERSION/bin/phantomjs /usr/local/bin/phantomjs
fi
