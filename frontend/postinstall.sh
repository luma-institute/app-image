#!/bin/bash
set -ex
paths="
./node_modules/ember-cli-sass/node_modules/broccoli-sass-source-maps/node_modules/node-sass/scripts/install.js
./node_modules/ember-cli-sass/node_modules/broccoli-sass-source-maps/node_modules/node-sass/scripts/build.js
./node_modules/node-sass/scripts/install.js
./node_modules/node-sass/scripts/build.js
"
for path in $paths; do
  if [[ -r "$path" ]]; then
    node "$path"
  fi
done
